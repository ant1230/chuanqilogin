//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////


declare var start:Function;
declare var Login:Function;
declare var LoginFunction:Function;
declare var showLoadProgress:Function;
declare var startLogin:Function;
declare var loginSuccessToLoading:Function;

class ListButton extends eui.ItemRenderer
{
    public dataInfo:any;
    public img:eui.Image;
    public label:eui.Label;

    constructor()
    {
        super();
    }
    public dataChanged(): void
    {
        let self = this;
        if(!self.dataInfo)
        {
            self.width = 199;
            self.height = 60;
        }
        
        if(!self.img)
        {
            self.img = new eui.Image("resource/login/xuanfutanchuang5.png");
            self.addChild(self.img);
        }
        if(!self.label)
        {
            self.label = new eui.Label();
            self.label.width = 165;
            self.label.y = 19;
            self.label.textColor = 14857728;
            self.label.size = 24;
            self.label.textAlign = egret.HorizontalAlign.CENTER;
            self.label.touchEnabled = false;
            self.addChild(self.label);
        }

        self.dataInfo = self.data;
        self.dataInfo.selection ? self.img.source = "resource/login/xuanfutanchuang4.png" : self.img.source = "resource/login/xuanfutanchuang5.png";
        if(self.dataInfo.index == 0)
            self.label.text = self.dataInfo.lab;
        else
            self.label.text = self.dataInfo.serAry[0].Zoneid + "-" + self.dataInfo.serAry[self.dataInfo.serAry.length - 1].Zoneid + "区"
       
    }
}

class Main extends eui.UILayer {
    public openId = "";
    public getType = 0;
    public serverlist = [];
    public myLoginList = [];
    public selectIdx = 0;
    public serverAry = [];

    public group:eui.Group;
    public groupLogin:eui.Group;
    public servGroup:eui.Group;
    public typeIcon:eui.Image;
    public serviceLabel:eui.Label;
    public clickLabel:eui.Label;
    public img4:eui.Image;
    public loadingGroup:eui.Group;
    public rect:eui.Rect;
    public img3:eui.Image;
    public label:eui.Label;
    public editableText:eui.EditableText;
    public createServiceGroup:eui.Group;
    public serviceRect:eui.Rect;
    public scroller:eui.Scroller;
    public list:eui.List;
    public scroller2:eui.Scroller;
    public list2:eui.List;
    public dataArray:eui.ArrayCollection;
    public selectSerInfo:any;
    public static init:any;
    public userid:number;

    protected createChildren(): void {
        super.createChildren();
        Main.init = this;
        let self = this;
        egret.Capabilities.isMobile && -1 == navigator.userAgent.indexOf("iPad") || (self.stage.scaleMode = egret.StageScaleMode.SHOW_ALL,
                    self.stage.orientation = egret.OrientationMode.AUTO);

        egret.lifecycle.addLifecycleListener((context) => {
        })

        egret.lifecycle.onPause = () => {
            egret.ticker.pause();
        }

        egret.lifecycle.onResume = () => {
            egret.ticker.resume();
        }
        self.group = new eui.Group();
        self.group.width = 720
        self.group.height = 1280;
        self.stage.addChild(self.group);
        var i = new eui.Image("resource/login/loaded.png");
        i.horizontalCenter = 0;
        self.group.addChild(i);
        self.groupLogin = new eui.Group;
        self.groupLogin.width = 720;
        self.groupLogin.height = 200;
        self.groupLogin.horizontalCenter = 0;
        self.groupLogin.bottom = 100;
        self.group.addChild(self.groupLogin);
        self.servGroup = new eui.Group;
        self.groupLogin.addChild(self.servGroup);
        self.servGroup.horizontalCenter = 0;
        var r = new eui.Image("resource/login/serbg.png");
        r.x = 0;
        r.y = 7;
        var s = new eui.Image("resource/login/serbg.png");
        s.x = 446;
        s.y = 7;
        s.scaleX = -1;
        self.servGroup.addChild(r);
        self.servGroup.addChild(s);
        self.typeIcon = new eui.Image("resource/login/fwq_hb.png");
        self.typeIcon.x = 57;
        self.typeIcon.y = 17;
        self.servGroup.addChild(self.typeIcon);
        self.serviceLabel = new eui.Label();
        self.serviceLabel.x = 103;
        self.serviceLabel.y = 19;
        self.serviceLabel.textColor = 14857728;
        self.serviceLabel.text = "5区";
        self.servGroup.addChild(self.serviceLabel);
        self.clickLabel = new eui.Label();
        self.clickLabel.x = 280;
        self.clickLabel.y = 19;
        self.clickLabel.textColor = 56833;
        self.clickLabel.textFlow = new Array({text: "点击换区", style: { underline: true }});
        self.clickLabel.addEventListener(egret.TouchEvent.TOUCH_TAP, self.onLabelClick, self);
        self.servGroup.addChild(self.clickLabel);
        self.img4 = new eui.Image("resource/login/btn_kaishi.png");
        self.img4.x = 70;
        self.img4.y = 67;
        self.servGroup.addChild(self.img4);
        self.img4.addEventListener(egret.TouchEvent.TOUCH_TAP, self.onButtonClick, self);
        self.loadingGroup = new eui.Group;
        self.loadingGroup.width = 533;
        self.loadingGroup.visible = false;
        self.groupLogin.addChild(self.loadingGroup);
        var o = new eui.Image("resource/login/jiazaitiao1.png");
        o.x = 24;
        o.y = 9;
        o.width = 488;
        self.loadingGroup.addChild(o);
        self.rect = new eui.Rect(488, 19, 1312e3);
        self.rect.y = 9;
        self.rect.right = 21;
        self.loadingGroup.addChild(self.rect);
        var n = new eui.Image("resource/login/jiazaitiao.png");
        n.width = 533;
        n.scale9Grid = new egret.Rectangle(37, 13, 2, 11);
        self.loadingGroup.addChild(n);
        self.img3 = new eui.Image("resource/login/jiazaitiaotx.png");
        self.img3.y = -7;
        self.img3.x = -5;
        self.loadingGroup.addChild(self.img3);
        self.label = new eui.Label,
        self.label.size = 20;
        self.label.textColor = 56833;
        self.label.y = -18;
        self.label.horizontalCenter = .5;
        self.label.text = "(正在加载主程序)";
        self.loadingGroup.addChild(self.label);
        self.loadingGroup.horizontalCenter = 0;
        console.log("loginType = " + window["loginType"]);
        if (0 == window["loginType"])
        {
            self.editableText = new eui.EditableText;
            self.editableText.width = 205;
            self.editableText.height = 40;
            self.editableText.text = "protest0100";
            self.editableText.size = 30;
            self.editableText.anchorOffsetY = 0;
            self.editableText.anchorOffsetX = 0;
            self.editableText.textAlign = "center";
            self.editableText.horizontalCenter = .5;
            self.editableText.bottom = 410;
            self.editableText.maxChars = 8;
            var l = egret.localStorage.getItem("userName");
            null != l && "" != l && (self.editableText.text = l);
            self.group.addChild(self.editableText);
            self.editableText.visible = false;
            self.onLabelClick(1);
        } else
        {
            //self.servGroup.visible = false;
            Login();
        }

        if(window["userData"] && window["userData"]["userId"])
        {
            this.userid = Number(window["userData"]["userId"]);
            self.servGroup.visible = false;
        }
            
    }

    public onLabelClick(e)
    {
        let self = this;
        self.getType = 0;
        1 == e && (self.getType = e);
        var t = new egret.HttpRequest();
        t.responseType = egret.HttpResponseType.TEXT;
        t.addEventListener(egret.Event.COMPLETE, self.getServiceComp, self);
        t.addEventListener(egret.IOErrorEvent.IO_ERROR, self.getServiceErr, self);
        /*0 == window["loginType"] ? t.open(window["serviceListdUrl"] + "?account=" + this.editableText.text,
            egret.HttpMethod.GET) : t.open(window["serviceListdUrl"] + "?account=" + this.openId, egret.HttpMethod.GET);
        t.send();*/
        //if(0 == window["loginType"])
        {
            t.open(window["serviceListdUrl"] , egret.HttpMethod.POST);
            var s = JSON.stringify({"request": "serverstate"});
            t.send(s);
        }

        
        self.clickLabel.touchEnabled = false;
    }
    public setServer()
    {
        let self = this;
        var e = new egret.HttpRequest;
        e.responseType = egret.HttpResponseType.TEXT;
        /*0 == window["loginType"] ? e.open(window["setServiceListdUrl"] + "?account=" + self.editableText.text + "&srvid=" + self.selectSerInfo.ID,
            egret.HttpMethod.GET) : e.open(window["setServiceListdUrl"] + "?account=" + self.openId + "&srvid=" + self.selectSerInfo.ID,
                egret.HttpMethod.GET);*/
        0 == window["loginType"] ? e.open(window["setServiceListdUrl"] + "?account=" + self.userid + "&srvid=" + self.selectSerInfo.ID,
            egret.HttpMethod.GET) : e.open(window["setServiceListdUrl"] + "?account=" + self.openId + "&srvid=" + self.selectSerInfo.ID,
                egret.HttpMethod.GET);
        e.send();
    }
    public getServiceComp(e)
    {
        let self = this;
        console.log("getServiceComp");
        var t = e.target;
        self.clickLabel.touchEnabled = true;
        //self.servGroup.visible = true;
        if (null == t.response || 0 == t.response || "" == t.response) 
            return alert("暂无服务器！");
        let r = [];
        let s = [];
        let o = [];
        let serList = JSON.parse(t.response);

        let len = serList.Data.length;
        for(let i = 0; i< len; i++)
        {
            //if(window["pf"] == serList.Data[i].pf)
                r.push(serList.Data[i]);
            if(serList.login && serList.login.length > 0 && -1 != serList.login.indexOf(serList.Data[i].ID + ""))
                o[serList.Data[i].ID + ""] = serList.Data[i];
            if (serList.login && serList.login.length > 0)
            {
                for (var m = 0; m < serList.login.length; m++)
                {
                    o[serList.login[i]] && s.push(o[serList.login[m]]);
                }
            }
        }
        //return;
        /*let i = JSON.parse(t.response);
        let len = i.serverlist.length;
        for (let l = 0; len > l; l++)
        {
            if(window["pf"] == i.serverlist[l].pf)
                r.push(i.serverlist[l]);
            if(i.login && i.login.length > 0 && -1 != i.login.indexOf(i.serverlist[l].ID + ""))
                o[i.serverlist[l].ID + ""] = i.serverlist[l];
            if (i.login && i.login.length > 0)
            {
                for (var m = 0; m < i.login.length; m++)
                {
                    o[i.login[l]] && s.push(o[i.login[m]]);
                }
            }
        }*/
        if(r.length == 0)
            alert("暂无服务器！");
        else
        {
            if(self.getType == 0)
                self.createServiceView({serAry: r,loginAry: s});
            else
            {
                if(s.length > 0)
                    self.onClickList2({ item: s[0] })
                else
                    self.onClickList2({ item: r[r.length - 1] });
            }
        }
    }
    public createServiceView(e)
    {
        let self = this;
        if(!self.createServiceGroup)
        {
            self.serviceRect = new eui.Rect(720, 1280, 0);
            self.serviceRect.alpha = .3;
            self.group.addChild(self.serviceRect);
            self.createServiceGroup = new eui.Group;
            self.createServiceGroup.width = 547;
            self.createServiceGroup.height = 753;
            self.createServiceGroup.horizontalCenter = 0;
            self.createServiceGroup.verticalCenter = 0;
            self.group.addChild(self.createServiceGroup);
            var t = new eui.Image("resource/login/xuanfutanchuang.png");
            t.scale9Grid = new egret.Rectangle(206, 72, 26, 26);
            t.width = 547;
            t.height = 753;
            self.createServiceGroup.addChild(t);
            var i = new eui.Label("选择区服");
            i.x = 221;
            i.y = 20;
            i.size = 26;
            i.textColor = 14857728;
            self.createServiceGroup.addChild(i);
            var r = new eui.Image("resource/login/xuanfutanchuang3.png");
            r.scale9Grid = new egret.Rectangle(11, 14, 4, 3);
            r.width = 193;
            r.height = 611;
            r.x = 35;
            r.y = 75;
            self.createServiceGroup.addChild(r);
            var s = new eui.Image("resource/login/xuanfutanchuang3.png");
            s.scale9Grid = new egret.Rectangle(11, 14, 4, 3);
            s.width = 267;
            s.height = 611;
            s.x = 245;
            s.y = 75;
            self.createServiceGroup.addChild(s);
            self.scroller = new eui.Scroller;
            self.scroller.width = 209;
            self.scroller.height = 598;
            self.scroller.x = 37;
            self.scroller.y = 81;
            self.createServiceGroup.addChild(self.scroller);
            self.list = new eui.List;
            self.list.itemRenderer = ListButton;
            var o = new eui.VerticalLayout;
            o.gap = 7;
            o.paddingTop = 8;
            o.paddingLeft = 10;
            self.list.layout = o;
            self.scroller.viewport = self.list;
            self.list.addEventListener(eui.ItemTapEvent.ITEM_TAP, self.onClickScrollCellHandler, self);
            self.scroller2 = new eui.Scroller;
            self.scroller2.width = 255;
            self.scroller2.height = 598;
            self.scroller2.x = 252;
            self.scroller2.y = 80;
            self.createServiceGroup.addChild(self.scroller2);
            self.list2 = new eui.List;
            self.list2.itemRenderer = ServiceItem;
            var o = new eui.VerticalLayout;
            o.gap = 5;
            o.paddingTop = 8;
            o.paddingLeft = 5;
            self.list2.layout = o;
            self.scroller2.viewport = self.list2;
            self.list2.addEventListener(eui.ItemTapEvent.ITEM_TAP, self.onClickList2, self);
            var t = new eui.Image("resource/login/chatclose.png");
            t.x = 473;
            t.y = 10;
            t.addEventListener(egret.TouchEvent.TOUCH_TAP, self.closeFuncton, self);
            self.createServiceGroup.addChild(t);
            var t = new eui.Image("resource/login/fwq_sc.png");
            t.x = 94;
            t.y = 702;
            self.createServiceGroup.addChild(t);
            var t = new eui.Image("resource/login/fwq_wh.png");
            t.x = 227;
            t.y = 702;
            self.createServiceGroup.addChild(t);
            var t = new eui.Image("resource/login/fwq_hb.png");
            t.x = 357;
            t.y = 702;
            self.createServiceGroup.addChild(t);
            var n = new eui.Label("顺畅");
            n.textColor = 2424576;
            n.x = 144;
            n.y = 709;
            n.size = 22;
            self.createServiceGroup.addChild(n);
            var n = new eui.Label("维护");
            n.textColor = 12303291;
            n.x = 276;
            n.y = 709;
            n.size = 22;
            self.createServiceGroup.addChild(n);
            var n = new eui.Label("爆满");
            n.textColor = 16733184;
            n.x = 408;
            n.y = 709;
            n.size = 22;
            self.createServiceGroup.addChild(n);
        }
        self.createServiceGroup.visible = true;
        self.serviceRect.visible = true;
        self.serverlist = e.serAry;
        self.serverAry = [];
        let len = self.serverlist.length;
        let h = 0; let a = 0;
        for(a = 0; a < len; a++)
        {
            h = Math.floor(a / 100);
            null == self.serverAry[h] && (self.serverAry[h] = { serAry: [] });
            self.serverAry[h].serAry.push(self.serverlist[a]);
        }
        self.serverAry.push({index: -1, lab: "最近登录", serAry: e.loginAry });
        self.serverAry = self.serverAry.reverse();
        len = self.serverAry.length;
        for(a = 0; a < len; a++)
        {
            self.serverAry[a].index = a;
        }
        self.serverAry[1].selection = true;
        self.dataArray = new eui.ArrayCollection(self.serverAry);
        self.list.dataProvider = self.dataArray;
        self.selectIdx = 1;
        self.onClickScrollCellHandler({ item: self.serverAry[1] });
    }
    public getServiceErr()
    {
        this.clickLabel.touchEnabled = true;
    }
    public closeFuncton()
    {
        let self = this;
        if(self.createServiceGroup)
        {
            self.createServiceGroup.visible = false;
            self.serviceRect.visible = false;
        }
        self.clickLabel.touchEnabled = true;
    }
    public onClickScrollCellHandler(e)
    {
        let self = this;
        if(self.serverAry[self.selectIdx])
            self.serverAry[self.selectIdx].selection = false;
        self.selectIdx = e.item.index;
        if(self.serverAry[self.selectIdx])
            self.serverAry[self.selectIdx].selection = true;
        self.dataArray.replaceAll(self.serverAry);
        self.list2.dataProvider = new eui.ArrayCollection(e.item.serAry);
    }

    public onClickList2(e)
    {
        let self = this;
        self.closeFuncton();
        self.selectSerInfo = e.item;
        /*if(self.selectSerInfo)
            self.serviceLabel.text = self.selectSerInfo.srvname;*/
        if(self.selectSerInfo)
        self.serviceLabel.text = self.selectSerInfo.Name;
    }
    public onButtonClick()
    {
        let self = this;
        if(!self.selectSerInfo)
            return alert("请选择服务器！");
        /*self.selectSerInfo.url = 'http://192.168.30.43:9001';
        self.selectSerInfo.ip = '192.168.30.43';
        self.selectSerInfo.port = '9001';*/
        self.selectSerInfo.srvid = 80;
        self.selectSerInfo.serverid = 80;
        self.setServer();
        self.servGroup.visible = false;
        self.loadingGroup.visible = true;
        var i = "?srvaddr=" + self.selectSerInfo.Ip + "&srvid=" + self.selectSerInfo.Zoneid + "&serverid=" + self.selectSerInfo.Zoneid + "&srvport=" + self.selectSerInfo.Port + "&user=";
        if(window["loginType"] == 0)
        {
            self.editableText.visible = false;
            /*egret.localStorage.setItem("userName", self.editableText.text);
            egret.localStorage.setItem("userName", self.editableText.text);
            egret.localStorage.setItem("startpram", self.editableText.text);
            start(i + self.editableText.text, self.selectSerInfo.Zoneid + "");*/
            egret.localStorage.setItem("userName", self.userid + "");
            start(i + self.userid, self.selectSerInfo.Zoneid + "");
            egret.localStorage.setItem("startpram", i);
        }
        else
        {
            start(i + self.openId, self.selectSerInfo.Zoneid + "");
            egret.localStorage.setItem("startpram", i + self.openId);
        }
    }
    public compHttp(e)
    {
        let self = this;
        var t = e.target;
        if (null == t.response || "" == t.response || "100" == t.response || 100 == t.response) 
        {
            alert("帐号创建失败！");
            self.img4.touchEnabled = true;
        }
        else {
            self.setServer(),
            self.servGroup.visible = false;
            self.loadingGroup.visible = true;
            var i = "?srvaddr=" + self.selectSerInfo.Ip + "&srvid=" + self.selectSerInfo.Zoneid + "&serverid=" + self.selectSerInfo.Zoneid + "&srvport=" + self.selectSerInfo.Port + "&user=";
            if(window["loginType"] == 0)
            {
                self.editableText.visible = false;
                /*egret.localStorage.setItem("userName", self.editableText.text);
                start(i + self.editableText.text, self.selectSerInfo.Zoneid + "");
                egret.localStorage.setItem("startpram", i + self.editableText.text);*/
                egret.localStorage.setItem("userName", self.userid + "");
                start(i + self.userid, self.selectSerInfo.Zoneid + "");
                egret.localStorage.setItem("startpram", i);
            }
            else
            {
                start(i + self.openId, self.selectSerInfo.Zoneid + "");
                egret.localStorage.setItem("startpram", i + self.openId);
            }
        }

    }
    public errHttp(e)
    {
        alert("服务器连接失败！");
        this.img4.touchEnabled = !0;
    }
    public showLoadProgress(e, i)
    {
        let self = this;
        self.rect.width = (100 - Number(e)) / 100 * 488;
        self.img3.x = Number(e) / 100 * 488 - 35;
        self.label.text = "" + i;
    }
    public LoginFunction(e)
    {
        let self = this;
        self.openId = e.openId;
        self.openId ? self.onLabelClick(1) : alert("非法登陆！");
    }
    public startLogin(e)
    {
        let self = this;
        self.editableText.visible = false;
        self.servGroup.visible = false;
        self.loadingGroup.visible = true;
        
        var t = new egret.HttpRequest();
        t.responseType = egret.HttpResponseType.TEXT;
        t.addEventListener(egret.Event.COMPLETE, self.loginComp, self);
        t.addEventListener(egret.IOErrorEvent.IO_ERROR, self.loginErr, self);
        {
            t.open(window["serviceLogindUrl"] , egret.HttpMethod.POST);
            var s = JSON.stringify({"openid": e.openid, "token": e.token, "label": "h5sdklogin", "zoneid": Number(e.zoneid)});
            t.send(s);
        }
    }

    public loginComp(e)
    {
        console.log("loginComp");
        var t = e.target;
        if (null == t.response || 0 == t.response || "" == t.response) 
            return alert("loginComp 未知错误");
        if(t.response == "success")
        {
            loginSuccessToLoading(egret.localStorage.getItem("startpram") + this.userid);
        }
    }

    public loginErr(e)
    {
        console.log("loginErr");
    }
    
}

class ServiceItem extends eui.ItemRenderer
{
    public typeAry;
    public dataInfo:any;
    public img:eui.Image;
    public label:eui.Label;
    public imgtype:eui.Image;

    constructor()
    {
        super();
        this.typeAry = ["resource/login/fwq_sc.png", "resource/login/fwq_hb.png", "resource/login/fwq_wh.png"];
    }
    public dataChanged(): void
    {
        let self = this;
        if(null == self.dataInfo)
        {   
            self.width = 244;
            self.height = 62;
            self.img = new eui.Image("resource/login/xuanfutanchuang2.png");
            self.img.scale9Grid = new egret.Rectangle(8, 7, 51, 48);
            self.img.width = 300;
            self.addChild(self.img);
            self.imgtype = new eui.Image;
            self.imgtype.x = 42;
            self.imgtype.y = 16;
            self.addChild(self.imgtype);
            self.label = new eui.Label;
            self.label.x = 99;
            self.label.y = 18;
            self.label.size = 24;
            self.label.touchEnabled = false;
            self.label.textColor = 14857728;
            self.addChild(self.label);
        }
        self.dataInfo = self.data;
        if(self.dataInfo)
        {
            self.imgtype.source = self.typeAry[self.dataInfo.status];
            self.label.text = self.dataInfo.Name;
        }
    }
}