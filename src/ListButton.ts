export class ListButton extends eui.ItemRenderer
{
    public dataInfo:any;
    public img:eui.Image;
    public label:eui.Label;

    constructor()
    {
        super();
    }
    protected dataChanged(): void
    {
        if(!this.dataInfo)
        {
            this.width = 199;
            this.height = 60;
            this.img = new eui.Image("resource/login/xuanfutanchuang5.png");
            this.addChild(this.img);
            this.label = new eui.Label();
            this.label.width = 165;
            this.label.y = 19;
            this.label.textColor = 14857728;
            this.label.size = 24;
            this.label.textAlign = egret.HorizontalAlign.CENTER;
            this.label.touchEnabled = false;
            this.addChild(this.label);
        }
        this.dataInfo = this.data;
        this.dataInfo.selection ? this.img.source = "resource/login/xuanfutanchuang4.png" : this.img.source = "resource/login/xuanfutanchuang5.png";
        if(this.dataInfo.index == 0)
            this.label.text = this.dataInfo.lab;
        this.label.text = this.dataInfo.serAry[0].ID + "-" + this.dataInfo.serAry[this.dataInfo.serAry.length - 1].ID + "区"
       
    }
}